function enviarMensaje() {
    var mensaje = document.getElementById("textInput").value.trim();
    if (mensaje === "") {
        return;
    }

    mostrarMensajeUsuario(mensaje);
    mostrarMensajeBot("Estoy procesando tu consulta. Por favor, espera un momento...");

    fetch('http://localhost:11434/api/generate', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            model: "llama2",
            prompt: "Quiero que respondas estrictamente en español como el mejor psicólogo del mundo a lo que te voy a decir: " + mensaje,
            stream: false
        })
    })
    .then(response => response.json())
    .then(data => {
        mostrarMensajeBot(data.response);
    })
    .catch(error => {
        console.error('Error al obtener respuesta del chatbot:', error);
        mostrarMensajeBot("Lo siento, no pude obtener una respuesta en este momento.");
    });

    document.getElementById("textInput").value = "";
}

function mostrarMensajeUsuario(mensaje) {
    var chatMessages = document.getElementById("chat-messages");
    var messageElement = document.createElement("div");
    messageElement.textContent = mensaje;
    messageElement.classList.add("user-message");
    chatMessages.appendChild(messageElement);
}

function mostrarMensajeBot(mensaje) {
    var chatMessages = document.getElementById("chat-messages");
    var messageElement = document.createElement("div");
    messageElement.textContent = mensaje;
    messageElement.classList.add("bot-message");
    chatMessages.appendChild(messageElement);
    chatMessages.scrollTop = chatMessages.scrollHeight;
}
